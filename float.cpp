#include <iostream>

int accuracy() {
    int a = 0;
    float acc1 = 1;//set a float point variable
    //double acc2 = 1;
    while (acc1 + 1 != 1) {
        acc1=acc1/2;
        --a;
    }
    return a + 1;
}


int main() {
    std::cout << "Machine accuracy for float: 2^" << accuracy() << '\n';
   // std::cout << "Machine accuracy for double: 2^" << accuracy() << '\n';
}
