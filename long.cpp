#include <iostream>

int accuracy() {
    int a = 0;
    //set a long variable
    long double acc3 = 1;
    while (acc3 + 1 != 1) {
        acc3=acc3/2;
        --a;
    }
    return a + 1;
}


int main() {
    //std::cout << "Machine accuracy for float: 2^" << accuracy() << '\n';
    std::cout << "Machine accuracy for long double: 2^" << accuracy() << '\n';
}
