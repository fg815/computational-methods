#include <iostream>

int accuracy() {
    int a = 0;
    //set a double variable
    double acc2 = 1;
    while (acc2 + 1 != 1) {
        acc2=acc2/2;
        --a;
    }
    return a + 1;
}


int main() {
    //std::cout << "Machine accuracy for float: 2^" << accuracy() << '\n';
    std::cout << "Machine accuracy for double: 2^" << accuracy() << '\n';
}
